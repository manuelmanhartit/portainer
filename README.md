# Portainer 1.0

This runs a [Portainer][5] instance to manage your docker services through a web UI.

You can use it to manage all your docker containers and even the [docker-compose creations][7].

Also you can create a service (called [Stack][7]) through connecting to your compose file in a [git repo][8] and autoupdate a service if the repo changes. So you can easily fully automate your deployment chain.

## Getting Started

For starting the service initially just copy the `.env-example` to `.env` and edit the variables so they fit your environment.

After that, you can start the container.

### Upgrading the service / containers

For upgrading the container, you will need to call

	# docker-compose down; docker-compose pull; docker-compose up -d

If you put in your dyndns settings in another way then via `.env` please ensure that it still works with the new container.

## About the project

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository](https://bitbucket.org/mmprivat/selfhost-updater/downloads/?tab=tags).

### Author(s)

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Contributing

If you want to contribute, contact the author or create a pull request.

### Changelog

__1.0__

* Created a new bitbucket repo for the project

### Open issues

-

## Sources

* [Docker][1] - The container base
* [Docker-Compose][2] - Composing multiple containers into one service
* [VS Code][3] - Used to edit all the files
* [Docker Image][4]
* [Portainer][5]
* [Run & Update Portainer with Docker-Compose][6]
* [Docker-Compose inside Portainer via Stacks][7]
* [Create a stack][8]
* [Why Portainer needs 2 port mappings][9]

[1]: http://www.docker.io/
[2]: https://docs.docker.com/compose/
[3]: https://code.visualstudio.com/
[4]: https://hub.docker.com/repository/docker/mmprivat/selfhost-updater
[5]: https://www.portainer.io/
[6]: https://joettis.com/blog/2020/08/18/einfache-aktualsieriung-von-portainer-io-mittel-docker-compose-yml/
[7]: https://www.portainer.io/blog/stacks-docker-compose-the-portainer-way
[8]: https://docs.portainer.io/v/ce-2.11/user/docker/stacks/add
[9]: https://github.com/portainer/portainer-docs/issues/91